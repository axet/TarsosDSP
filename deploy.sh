#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ARGS=(-DpomFile="$DIR/pom.xml" -Dfile=TarsosDSP-2.4-bin.jar -Dsources=TarsosDSP-2.4.jar -DgroupId=com.github.axet -Dversion=2.4-1 -DartifactId=TarsosDSP -Dpackaging=jar -DrepositoryId=sonatype-nexus-staging   -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/)

mvn install:install-file "${ARGS[@]}"

mvn gpg:sign-and-deploy-file "${ARGS[@]}"
